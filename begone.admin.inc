<?php

/**
 * @file
 * Admin page callbacks for the begone module.
 */

/**
 * Menu callback for the begone module to display its administration screen
 */
function begone_admin($form, $form_state, $lang = NULL) {

// variable_del("begones");

  // Setup the form
  $form['#cache'] = TRUE;
  $form['#attached']['css'][drupal_get_path('module', 'begone') . '/begone.admin.css'] = array();

  $form['string'] = array(
    '#tree' => TRUE,
    '#theme' => 'begone_strings',
  );

  // Retrieve the begone data from the variables table.
  $begone_var = variable_get("begones", array());

  // See how many begones rows there should be.
  $string_count = 0;
  if (isset($form_state['string_count'])) {
    $string_count = $form_state['string_count'];
  }
  else {
    $string_count = count($begone_var) ;
  }

  //Sort the strings and display them in the form.

  for ($index = 0; $index < $string_count; $index++) {
    if (isset($begone_var[$index])) {
      $string = $begone_var[$index];
      $form['string'][$index] = begone_textbox_combo($index, $string['enabled'], $string['selector']);

    }
    else {
      $form['string'][$index] = begone_textbox_combo($index, -1);
    }
  }


  // Add the buttons to the form.
  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions', 'container-inline')),
  );

  $form['actions']['more_strings'] = array(
    '#type' => 'submit',
    '#value' => t('Add Begone'),
    '#description' => t("Click here to add more Begone rows."),
    '#weight' => 2,
    '#submit' => array('begone_more_strings_submit'),
    '#ajax' => array(
      'callback' => 'begone_ajax',
      'wrapper' => 'begone-wrapper',
      'method' => 'replace',
      'effect' => 'none',
    ),
  );

  $form['actions']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save Begones'),
    '#weight' => 3,
  );
  $form['actions']['remove'] = array(
    '#type' => 'submit',
    '#value' => t('Remove disabled Begones'),
    '#weight' => 4,
    '#access' => !empty($begone_var),
  );

  return $form;

}


/**
 * Triggered when the user submits the administration page.
 */
function begone_admin_submit($form, &$form_state) {
  if (!in_array($form_state['clicked_button']['#id'], array('edit-save', 'edit-remove'))) {
    // Submit the form only for save and remove buttons.
    return;
  }

  // Store begones in the database.

  $words = array();

  foreach ($form_state['values']['string'] as $index => $string) {
    $words[$index] = $string;
  }

  variable_set("begones", $words);

  // Save the values and display a message to the user .
  switch ($form_state['clicked_button']['#id']) {
    case 'edit-save':
      variable_set("begones", $words);
      drupal_set_message(t('Your changes have been saved.'));
      break;

    case 'edit-remove':
      delete_begones($words);
      $words = array_values($words);
      variable_set("begones", $words);
      drupal_set_message(t('The disabled begones have been removed.'));
      break;
  }
}

/**
 * Function to return a textbox combo form.
 */
function begone_textbox_combo($delta = 0, $enabled = TRUE, $selector = '') {
  $form['#tree'] = TRUE;

  $form['enabled'] = array(
    '#type' => 'checkbox',
    '#default_value' => ($enabled == -1) ? TRUE : $enabled,
    // Have access if it's not a placeholder value.
    // '#access' => $enabled != -1,
    '#attributes' => array(
      'title' => t('Begone!'),
    ),
  );

  $form['selector'] = array(
    '#type' => 'textfield',
    '#default_value' => $selector,
    '#rows' => 1,
    '#attributes' => array(
      'title' => t('The text to replace the original source text.'),
    ),

  );

  return $form;
}

/**
 * Theme
 */

function theme_begone_strings($variables) {
  $form = $variables['form'];
  $rows = array();
  foreach (element_children($form) as $key) {
    // Build the table row.
    $rows[$key] = array(
      'data' => array(
        array('data' => drupal_render($form[$key]['enabled']), 'class' => 'begone-enabled'),
        array('data' => drupal_render($form[$key]['selector']), 'class' => 'begone-selector'),
      ),
    );

    // Add any attributes on the element to the row, such as the ahah class.
    if (array_key_exists('#attributes', $form[$key])) {
      $rows[$key] = array_merge($rows[$key], $form[$key]['#attributes']);
    }
  }

  $header = array(
    array('data' => t('Selector Enabled'), 'title' => t('Begone!')),
    array('data' => t('Selector can be CSS classes (.) or ids (#) '), 'title' => t('Begone css selector identifier')),
  );

  $output = theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => 'begone-wrapper'),
  ));

  $output .= drupal_render_children($form);

  return $output;
}

/**
 * Menu callback; Display a new string override.
 */
function begone_ajax($form, &$form_state) {
  return $form['string'];
}

/**
 * Submit handler; The "Add row" button.
 */
function begone_more_strings_submit($form, &$form_state) {
  $form_state['string_count'] = count($form_state['values']['string']) + 1;
  $form_state['rebuild'] = TRUE;
}

/**
 * Sorts two words based on their source text.
 */
function begone_admin_word_sort($word1, $word2) {
  return strcasecmp($word1['source'], $word2['source']);
}


function delete_begones(&$words) {

  foreach ($words as $index => $rowdata) {
    if ( !$rowdata['enabled']) {
      unset($words[$index]);
    }
  }

return $words;

}
