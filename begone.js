
//on page load retrieve gloval status from drupal js object
// set class to control display of toggle button

jQuery(document).ready(function(){

begone = Drupal.settings.begone_status;

if (begone == "off") {
  jQuery("body").prepend('<a class="begone-toggle begone-off" href="#"></a>');
}
else {
  jQuery("body").prepend('<a class="begone-toggle begone-on" href="#"></a>');
}

begone = begone_display(begone);


// onlclicking begone toggle call toggle function
// and then set show/hide for each selector

jQuery(".begone-toggle").click(function() {

begone = begone_toggle(begone);
begone = begone_display(begone);

}

);


});


//Toggle begone global status and variable_set via menu callback

function begone_toggle (begone) {

if (begone == 'off') {

  jQuery(".begone-toggle").toggleClass('begone-off');
  jQuery(".begone-toggle").toggleClass('begone-on');
  jQuery(".admin-menu").toggleClass('admin-menu-begone');

   for (var i = 0; i < Drupal.settings.begonejs.length; i++) {

    if (Drupal.settings.begonejs[i].enabled) {

      jQuery(Drupal.settings.begonejs[i].selector).hide();
    }
      begone = 'on';
   }
}

else {

  jQuery(".begone-toggle").toggleClass('begone-on');
  jQuery(".begone-toggle").toggleClass('begone-off');

  for (var i = 0; i < Drupal.settings.begonejs.length; i++) {
      jQuery(Drupal.settings.begonejs[i].selector).show();
   }
   begone = 'off';

}


jQuery.get('/toggle_begone_link', function() {
   $('.result').html();

});


return begone;

} ;


// Hide all begones that are enabled (if enabled globally).

function begone_display (begone) {

if (begone == 'on') {

  // jQuery(".admin-menu").toggleClass('admin-menu-begone');

   for (var i = 0; i < Drupal.settings.begonejs.length; i++) {

    if (Drupal.settings.begonejs[i].enabled) {

      jQuery(Drupal.settings.begonejs[i].selector).hide();

    }

   }
}

else {

  for (var i = 0; i < Drupal.settings.begonejs.length; i++) {
      jQuery(Drupal.settings.begonejs[i].selector).show();
   }

}

return begone;

}
